<?php
  if ( is_active_sidebar('subsidiary') ) : { //if there are widgets ?>
  <div id="sidebar-subsidiary" class="sidebar">
    <ul>
      <li>
        <?php dynamic_sidebar(subsidiary); // display the widgets ?>
      </li>
    </ul>
  </div>
<?php  } else : { // do something if there aren't widgets ?> 
<!-- If you don't have any widgets, nothing will appear in your subsidiary sidebar area. -->
<?php };
endif; ?>
