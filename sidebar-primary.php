      <?php
        if ( is_active_sidebar('primary') ) : { //if there are widgets ?>
        <div id="sidebar-primary" class="sidebar">
          <ul>
            <li>
              <?php dynamic_sidebar( 'primary' ); // display the widgets ?>
            </li>
          </ul>
        </div>
<?php } else : { ?>
        <div id="sidebar-primary" class="sidebar">
          <ul>
            <li>
              <div id="vertnav" class="ddsmoothmenu-v">
                <ul>
                	<?php wp_list_pages('sort_column=menu_order&depth=2&title_li=');?>               
                </ul>
            </div>
            <div id="searchBox">
              <?php get_search_form(); ?>
            </div>
          </li>
        </ul>
      </div>
   <?php
          };
        endif;
       ?>