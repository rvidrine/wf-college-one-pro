<?php
/**
 * @package Profile_Change_Notify
 * @version 1.0.1
 */
/*
Plugin Name: Profile Change Notify
Plugin URI: http://vidrinmr.capeville.wfunet.wfu.edu/plugins/profile_change_notify/
Description: This plugin sends a simple notification email to the admin user and optionally any other registered user when a profile on the site has been changed.
Version: 1.0
Author: Robert Vidrine
Author URI: http://college.wfu.edu/itg/about-us-2/staff/robert-vidrine
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify 
it under the terms of the GNU General Public License as published by 
the Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful, 
but WITHOUT ANY WARRANTY; without even the implied warranty of 
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
GNU General Public License for more details. 

You should have received a copy of the GNU General Public License 
along with this program; if not, write to the Free Software 
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA 
*/
/* taken from http://wordpress.org/support/topic/notify-admin-when-user-updates-profile
* This sends an email to the administrator when a user updates their profile. */
if (! function_exists( 'user_profile_notify' ) ){ // check to see if this function has already been called in the child theme and if so, skip the rest of the code.
	function user_profile_update_notify($userid) {
		$userdata = get_userdata($userid);
		$first_name = $userdata->first_name;
		$last_name = $userdata->last_name;
		$message = "A user profile has been updated\n\n";
		$message .= "User Profile for " . $first_name . " " . $last_name . " was just updated.";
		@wp_mail( array( get_option( 'admin_email' ) ), get_option( 'blogname' ) . ' User Profile Updated', $message); // to add an email, add a comma and another address, in single quotes in the first argument.
	}
	add_action('profile_update','user_profile_update_notify');
}
?>