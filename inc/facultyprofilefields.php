<?php
/* Add some fields to the user profile, specifically for recording faculty contact info
* These new fields are used in the facultyprofile.php page template
* Big Thanks to Otto Wood (yup, THAT Otto!) for helping me with the add_filter stuff */
add_filter( 'user_contactmethods', 'add_faculty_fields' );
function add_faculty_fields( $contact_methods ) {
	$contact_methods['wfco_ophone'] = 'Office Phone';
	$contact_methods['wfco_olocation'] = 'Office Location';
	$contact_methods['wfco_research_interests'] = 'Research Interests';
	// $contact_methods['jabber'] = ''; // just removes label, have to find how to remove it.
	return $contact_methods;
};
?>