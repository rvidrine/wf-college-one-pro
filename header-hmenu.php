<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title(''); ?> | <?php bloginfo( 'name' ); ?> | Wake Forest University</title>

<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<script type="text/javascript">
var templateDir = '<?php bloginfo("template_directory"); ?>';
</script>

<?php wp_enqueue_script('jquery', 'jquery-ui-tabs'); ?>
<?php wp_enqueue_script('ddsmoothmenu', get_bloginfo('template_directory') . '/js/ddsmoothmenu/ddsmoothmenu.js'); ?>

<?php wp_head(); ?>
<!-- Dynamic Drive Menu code -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/ddsmoothmenu/ddsmoothmenu.css" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/js/ddsmoothmenu/ddsmoothmenu-v.css" />

<!--
/***********************************************
* Smooth Navigational Menu- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
***********************************************/
-->
<script type="text/javascript">
ddsmoothmenu.init({
	mainmenuid: "hnav", //Menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	customtheme: ["#C1B58A", "#E7DCBA"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})

</script>
</head>

<body <?php body_class('wfCollegeOne'); ?>>	

<div id="container">
	<div id="header">
		<?php get_sidebar('header'); ?>
		<img src="<?php header_image(); ?>" width="<?php echo HEADER_IMAGE_WIDTH; ?>" height="<?php echo HEADER_IMAGE_HEIGHT; ?>" alt="Header image" />
		<div id="blogTitle"><?php echo get_option( 'blogname' ); ?></div>
		<div id="tagline"><?php echo get_option( 'description' ); ?></div>
		<div id="montage"> <!-- CSS supplies background image --> </div>
	</div>
	<div id="hnavcontainer">
		<div id="hnav" style="width:100%;">
                <ul>
                	<?php wp_list_pages('sort_column=menu_order&depth=2&title_li=');?>               
                </ul>
		</div>
</div>
