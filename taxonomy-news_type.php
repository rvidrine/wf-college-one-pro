<?php get_header(); ?>
<?php get_sidebar(primary); ?>

  <div id="mainContent">
    <div class="wfCollegeOne">
		<h1><?php single_term_title( 'News Type: ' ); ?></h1>

      <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
      <?php $args = array( 'post_type' => 'wfco_dept_news', 'posts_per_page' => 10, 'paged' => $paged );
      $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); ?>
	<div class="entry-content">
        <div class="news-item">
    <?php the_title('<h2><a href="' .get_permalink(). '">', '</a></h2>'); ?>
            <p class="posted_date">
              Posted on: <?php the_time(get_option('date_format')); ?>
			</p>
        <span class="post_thumbnail">
        	<?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<?php  the_post_thumbnail(); ?>
		</a>
		<?php } 	
		?>
        </span>
			<?php	the_excerpt(); ?>
			<p class="newstypes">
				<?php echo get_the_term_list( $post->ID, 'news_type', 'Related News: ', ' &bull; ', '' ); ?>
			</p>
        </div>
	</div>
      <?php endwhile; ?>
                    
    </div>
  </div>
<div class="navigation">
  <div class="alignleft"><?php next_posts_link('Previous 10 news items') ?></div>
  <div class="alignright"><?php previous_posts_link('Next 10 news items') ?></div>
</div
<!-- This is an HTML comment -->

<?php get_footer(); ?>
