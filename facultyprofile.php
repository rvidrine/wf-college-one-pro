<?php
	/*
		Template Name: Faculty Profile from WP Profile
	*/
?>
<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>
            <div id="mainContent">
                <?php if (have_posts()) : ?>
			
             			 
                        <?php while (have_posts()) : the_post(); ?>
                        
                            <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                            <?php 
								$deacnet_name = get_post_meta( $post->ID, 'deacnet_name', 'true'); // get their LDAP username as entered in custom field
								$user_profile_data = get_user_by( 'email', $deacnet_name . '@wfu.edu' ); // add username to domain, then get user data by email
								$first_name = $user_profile_data->first_name;
								$last_name = $user_profile_data->last_name;
								$email = $user_profile_data->user_email;
								$website = $user_profile_data->user_url;
								$phone = $user_profile_data->wfco_ophone; // get value of field added in functions.php
								$office = $user_profile_data->wfco_olocation; // another field added
								$research = $user_profile_data->wfco_research_interests; // many people won't populate this field, display below is conditional
							?>

                                <div class="entry">
									<div id="profArea">
										<div id="profName"><?php echo $first_name . ' ' . $last_name; ?></div>
										<div id="profPicArea">
											<div id="profPicIndent"></div>
										</div>
										<div class="profContact">
											<div id="office">Office: <?php echo $office; ?></div>
											<div id="email">Email: <?php echo '<a href="' . $email . '" >' . $email . '</a>'; ?></div>
											<div id="phone">Phone: <?php echo $phone; ?></div>
											<div id="website"><?php if( $website ) {
												echo 'Website: ' . '<a href="' . esc_url( $website ) . '">' . $website . '</a>'; }; ?></div>
											<div id="research"><?php if( $research ) {
												echo 'Research Interests: ' . $research; }; ?></div>
										</div>
									</div>
								
                                    <div id="additionalprofinfo"><p>More about me: </p><?php the_content('Read the rest of this entry &raquo;'); ?></div>
                                </div>
                                
                            </div>
                            
                        <?php endwhile; ?>
                        
                <?php else : ?>
                <?php endif; ?>
                </div>
            </div>
<?php get_footer(); ?>
