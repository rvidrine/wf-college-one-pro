<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
                <?php if (have_posts()) : ?>
                
                    <?php while (have_posts()) : the_post(); ?>
                    
                        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                            <div class="entry">
	<?php the_title('<h1><a href="' .get_permalink(). '">', '</a></h1>'); ?>

                                <?php the_content('Read the rest of this entry &raquo;'); ?>
                            </div>
                            
                        </div>
                        
                    <?php endwhile; ?>
                    
                 <?php else : ?>
                 <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
