<?php get_header(); ?>
<?php
  if ( is_home() ) :
      get_sidebar('home');
  elseif ( is_404() ) :
      get_sidebar('404');
  else :
      get_sidebar('primary');
  endif;
?>
    <?php get_sidebar('secondary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
		<?php if (have_posts()) : ?>
		
			<?php while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				
					<div class="entry">
						<?php the_title('<h1 class="page-title">', '</h1>'); ?>
<!-- display list of sub-pages, if the post content is empty -->
						<?php
						  $children = wp_list_pages( "title_li=&child_of=".$post->ID."&echo=0&depth=1" );
						  if ( empty( $post->post_content ) ) {
						 ?>
						  <ul>
						  <?php the_title('<h3>', ' Links</h3>'); ?> 
						  <?php echo $children; ?>
						  </ul>
						  <?php }
						  else
						?>
<!-- end of show children bit (from codex.wordpress.org) -->

                                <?php the_content('Read the rest of this entry &raquo;'); ?>
                            </div>
                            
                        </div>
                        
                    <?php endwhile; ?>
                    
                 <?php else : ?>
                 <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
