This theme was created by Robert Vidrine for use on official Wake Forest College pages within Wake Forest University websites. The theme includes logos and such which are copyrighted by Wake Forest University and are not approved for use on anything but official departmental pages. This theme uses the Hybrid Core framework from http://www.themehybrid.com/hybrid-core/. This theme was adapted from WFCollegeOne, also by Robert Vidrine.

Changelog:
1.3.6
	Edited style.css to remove some comments from when it was created from .scss with SASS (may return to that later).
	Added extra blank lines to improve readability
	Added TOC in style.css to make it easier to navigate
	Added rule to hide the h1.page-title on the home page.
1.3.5
	Changed inc/update.php to incluee new function to whitelist my update server.
1.3.4
	Some superficial cleanup of comments to prepare for public release. (style.css and readme.txt)
	Enabled the setting in the custom header code to disable showing the title and
	description (functions.php)
	Put default values into get_theme_mod calls in Customize_CSS function so that default values will get passed to CSS if no theme_mod of that name has ever been saved.
1.3.3b
	Forgot to put a comma back in the DD Smooth Menu init code when I took the Theme Customizer DDSM menu direction stuff back out.
	Corrected the syntax on the montage image selector code to include the url() syntax.
1.3.2
	Decided to roll back the DD Smooth Menu orientation stuff temporarily until I figure out the best way to offer people the ability to change the menu orientation, or if this is even necessary (since there is a horizontal template as well). (header.php and theme-options.php)
1.3.1
	Edited header-hnav.php so that it matched the header.php in all ways except for horizontal nav menu (including removing the comment-reply script from header-hnav and removing fontsizes.css which is called from style.css now.)
	Changed horizontalmenupagewide.php to more closely match page.php where it doesn't affect alignment: added page-title, added show children of top-level pages with no content.
1.3
	Fixed code for theme-options.php (functions.php).
	Set a default image for the header image (functions.php).
	DDSmoothMenu plugin is not working yet, moved back to the header.php.
	Added chunk of code to functions.php from Twenty Twelve's inc/custom-header.php to hide the title and description if the checkbox in customizer is unchecked.
	Added wp-head-callback of wfcopro_header_style.
	Added Theme Customizer option to change DD Smooth Menu init value from 'h' to 'v'. (modified header.php and inc/theme-options.php)
1.2.1b
	Moved theme customizer stuff to inc/theme-options.php.
	Moved DD Smooth Menu to a plugin instead of loaded directly in the header.php.
	Reset all sidebars to empty on theme load using wp_set_sidebars_widgets (should only fire once).
1.2
	Changed the functions.php to add theme customizer options for both changing the title text and show or hide the montage image.
1.1.4
	Changed inc/update.php to fix the "always needs update" problem. Now the theme version check runs correctly. Thanks Tommy for your help!
1.1.3
	Updated archive.php, archive-wfco_dept_news.php and functions.php to improve the way that Featured Images show, so that this works out of the box. (I also cleaned up the code formatting a bit to make it more readable.)
1.1.2
	Added code to archive-wfco_dept_news.php and archive.php to check for post thumbnail (AKA Featured Image) and display it before te excerpt if present.
	Rewrote many CSS selectors to optimize them. Generally, removed tag names from ID selectors and class selectors. I don't expect this to break anything, but should have some slight optimizing effect and may make writing selectors much more simple.
	Began using SASS to constuct stylesheets. (All styles are in one file.)
	Removed call to fontsizes.css from the header.php. Instead imported fontsizes.css into main style sheet (less complex to override).
1.1.1
	Added code to functions.php to call inc/facultyprofilefields.php, which adds Office Phone, Office Location and Research Interests to the user profile.
	Added facultyprofile.php page template to display the profile information for the user (based on email address) once their deacnet username is entered into a deacnet_name custom field.
1.1
	Fixed syntax error in sidebar-primary.php where term primary was not in single quotes, so appeared to be a constant.
	Fixed syntax error in header.php where term name was not in single quotes, so appeared to be a constant.
	Changed child page display code in page.php to avoid an undefined variable on pages that have content (and thus don't display the child pages in a list).
	Upgraded the hybrid core framework to 1.4.3.
	Removed added left margin for first-child of #pastlinks (breadcrumb id), which had fixed a bug with hybrid's breadcrumb code, which was now fixed in version 1.4.3 of hybrid core.
	Stripped out custom header code, switched to add_theme_support( 'custom_header' ); instead.
	Made register_default_headers pluggable, so if it is called in a child theme, it won't be called in WF College One Pro again.
1.0.2
  Fixed error in sidebar-primary.php where the wrong id (sidebar-header) was being applied to widgets dropped into primary sidebar area.
1.0.1
  Added screenshot.png to theme directory, for preview image in theme selector screen.
1.0
  Added theme support for custom backgrounds. Changed functions.php.
  Removed background color from body, html rule in styles.css. (Default background color now set in functions.php as part of custom background code.)
0.9.7
 Added code to functiuons.php to remove some Dashboard items that relate only to blog sites, such as QuickPress, WP blog, recent comments, recent drafts. Code is pluggable so can be overridden in child theme's functions.php by calling remove_dashboard_cruft.
0.9.6
  Made News Items post type pluggable (override-able in the child theme. Just redefine the register_post_type in the child theme and it will override the definition in the parent theme. Changed functions.php.
  Moved an ending curly brace to enclose custom news_type taxonomy within pluggable wfco_create_news_post_type function call.
  Corrected typo in function name on line 125 (function was misspelled as wfco_create_news_pos_type).
  Added rules for private class and for combination of logged-in and private class. (Don't display items with private class, unless preceded by lgged-in class.)
  Changed login/logout link at top left in default header sidebar area to return one back to the page from which they clicked the login link, instead of redirecting them to the front page.
0.9.5
  fixed footerwithbreadcrumbs.php to call subsidiary sidebar, supporting dynamic subsidiary footer code.
  Removed top and bottom margin of ul element directly within div.sidebar-subsidiary and div.sidebar-header.
0.9.4
  Added CSS rule to remove padding-left and bullet from ul and li elements directly within a sidebar div.
0.9.3
  Added li to widgetized footer and header areas for valid HTML.
  Made some code formatting changes to sidebar-primary and sidebar-secondary to make code more readable and consistent with other sidebars in the theme.
  Created sidebar-subsidiary to be consistent with other sidebars, instead of putting sidebar code into footer.php.
  Updated hybrid-core framework to 1.2.1
0.9.2
  Added min-height to div.entry so that excerpts with little text won't cause subsequent excerpts to appear indented within the short excerpt.
  Updated single.php (single blog posts) to include Posted On date and Related Posts category links below the post.
  Added support for hybrid core get_the_image to support post thumbnails in functions.php.
  Added home.php (template automatically used for blog) which includes support for featured images, categories below the post excerpt, and previous/next posts links at bottom.
0.9.1
  Somehow, new sidebar-primary.php didn't get copied into the previous release. New sidebar-primary, with WP search form now included.
0.9.0
  Added custom search results page, showing excerpts of resulting pages.
  Modified primary sidebar to use WP search form instead of Google search.
  Modified header sidebar to include a login/logout link and a register/site admin link when appropriate. (Login link appears if not logged in. When logged in, turns to logout link. When logged out, if anyone can register on the site, a registration link shows up. When logged in, a site admin link appears instead of 'register' link.)
  Applied loginout class to new login/logout conditional link (so it can be hidden or styled easily).
  Applied register_link class to new "register/site admin" link (depending on whether logged in or not and whether "anyone can register" option is enabled) so it can easily be hidden or styled with CSS.
  When widgets are inserted into header widget area, all default content is removed (login/logout, register/site admin, and default links to College and University).
0.8.5
  This version is just a testing version, and is identical to version 0.8.5, except for the version number in the style.css and this readme.txt. This is just used to test to make sure the theme can update successfully from 0.8.4 to 0.8.5.
0.8.4
  Added automatic update code from Konstruktors.com, written by Kaspars Dambis.
0.8.3
  Removed frontpage.php ("Front page with rotating graphic") template, because it was outdated and didn't include any widget areas.
  Removed searchbox.txt as this was just code that had been culled from the theme, but saved so I could potentially use it later.
0.8.2
  Removed peopleposttype.txt as this feature is not likely to be used in the foreseeable future. (This was coded tested but then removed from the functions.php.)
  Bug in pagination code in archive.php. Removed new archive.php, left pagination code in archive-wfco_dept_news.php.
0.8.1
  Fixed pagination on wfco_dept_news archive and single pages.
  Added pagination code to archive.php.
  Reformatted code in archive-wfco_dept_news.php to make it more readable.
0.8.0
  Changed files: header.php, header-hmenu.php, sidebar-header.php (new), style.css, readme.txt (this file).
  Added a call to the header sidebar (widget area) within the header div in header.php and header-hmenu.php.
  Created sidebar-header.php, containing code to either show the widgets inserted into the header or nothing.
  Added styles to style.css to hide all H3 elements in the header widget area, and make the ul display inline (all links on the same line), floated to the right.
  Header widget area is specifically designed to hold Links as inserted with the Bookmarks widget.
0.7.1
  Changed height of hnavcontainer to 32px.
  Removed archive and single pages for People post type, since I doubt this post type will ever be needed.
  Added code to display news_type for each excerpt in News Item archive and single pages.
  Removed code to show child pages from News Items archive page.
  Removed conditionals for sidebars (have to re-add if someone wants a sidebar for certain pages, also have to add the sidebar.)
  Removed show children bit from index.php.
  Changed style.css, set height of header to 'auto'.
0.7.0
  Fixed the News Type taxonomy for News Items, where attempting to add a news type resulted in an error "Cheatin' huh?". The taxonomy name as used in the code (not the label) contained a space, which was allowed in WP 3.0 but disallowed starting in 3.1. Functions.php is only changed file in this update.
0.6.0
  Changed the loop in the archive.php file, making it a standard loop instead of a custom one (fixed problem with category links; the custom loop must have some syntax problem)
  Added to functions.php a function to determine if a page is ANY ancestor of a page with given ID. function is called is_tree and takes an argument of the page ID. This can be used in conditional statements to determine ancestors of a page (for specific sidebar/menu/header, etc.)
  Changed homewithnews.php to have class page-title added to the h1 tag in the body.
  Added page-title class to page.php
  Changed show children bit to show links to direct children (depth=1) if the page has no content (menu placeholder page).
  Added .page-title to single.php's title h1 tag.
  Added .page-title to single-wfco_dept_news.php's title h1 tag.
  Changed class of content div in single-wfco_dept_news.php to dept-news-item.
0.5.9
  Removed hybrid-core template hierarchy feature, as this was not working as expected, and was causing trouble (not using my post-type templates). This was a change to the functions.php file.
  Added calls to primary and secondary sidebars to single-wfco_dept_news.php.
  Removed "show child pages" from single templates.
  Created single.php template.
0.5.8
  Added primary menu to 404.php (call to sidebar('primary'), could be dynamic.)
  Removed permalink from the title of the home page with news template (linked to itself).
0.5.7
  Fixed problem in header-hmenu.php where path to ddsmoothmenu.js was incorrect.
  Removed the search.php since it's not formatted correctly for this purpose. (TODO)
  Added class="sidebar" to div in sidebar-primary.php, so all sidebars can be addressed with sidebar class.
  Removed sidebar-vertnav.php as the vertical DDSmoothMenu is contained as a default in sidebar-primary.php.
  Removed twosidebars.php as this was a template experiment that didn't work properly.
  Removed single.php as this was not working properly (probably needs to look more like page.php.)
0.5.6
  Changed header.php to use wp_title to better calculate the <title> of the page.
  Added page.php to better define the header which shows the name of the page.
0.5.5
	Removed People post type for now.
	Added template for faculty listing, included new sidebar-facultylist.
	Added required WP styles.
0.5.4
	Removed div#searchbox from header-hmenu.php. put in searchbox.txt
	Added calls to sidebar-primary and -secondary in archive.php.
0.5.3
	Added styles for rounded corners on sidebar-secondary.
	Added 10px left and right margin on sidebar-secondary.
0.5.2
  Created template for home page with news loop and sidebar-news to be called from that template (uses #sidebar-secondary styles).
  Added styles for #news-sidebar-title and added 5px left padding to sidebar-secondary.
  Added clear:both to #footer
  Moved div#sidebar-secondary to within the dynamic_sidebar area of the if.
0.5.1
  changed #sidebar1 to #sidebar-primary in style.css
  created rules for #sidebar-secondary in style.css
  Changed width of sidebar-primary to a percentage.
  Styled sidebar-secondary with white background, 10px left margin.
0.5
  Add People post type.
  Added new taxonomy of News Type, added this to News custom post type.
  Removed support for page attributes from News post type.
  Added support for turning on comments and trackbacks for news post type.
  Added new taxonomy of 'Dept. Relationship' to people post type.
  Created templates for archive-people.php and single-people.php.
  Added call to secondary sidebar to index.php.
0.4.1
  Formatted custom post type code in functions.php.
0.4
  Changed padding-top for #sidebar1 from 50px to 10px.
  Actually remembered to update style.css to reflect new version number...
0.3
  Added subsidiary sidebar area to footer, shown only when active with widgets.
  Added get_sidebar call to subsidiary sidebar to footer.php.
  Removed enqueuing of comment-reply for singular pages (don't need these now, may for blog later).
  Added style rule to remove padding and margin from ul in #footer-sidebar.
  Created sidebar-primary.php, put vertnav stuff in as fallback for no primary widgets installed in Primary.
  Changed index.php to call Primary sidebar instead of vertnav.
  Added 15px margin-top to #footer.
0.2
  Added a sidebar called sidebar-vertnav.php to hold the vertical navigation menu, made to work with DDSmoothmenu from Dynamic Drive.
  Added a search template(currently not working) which holds the searchform div from the previous theme, which had been in the header.
  Repaired some code in the header which called the DDSmoothmenu javascript from the wfcollegeone theme directory (now calls it from template_directory)
  Added a couple of templates for use with NextGEN Gallery, one so the gallery will work correctly, and another for a front page random image div. Front page random image div is hard-coded to accept images 400x300px. Front page random template is modification of gallery-carousel template, with previous and next image links removed (since these apparently call the posts loop or something, haven't gotten that working).
  nggallery templates have to be called within nggallery shortcodes, like [nggallery id=x template=frontpagerandom].
  nggallery template frontpagecarousel currently not working, still contains next/previous links which don't work.
  Added theme support for primary, secondary, header, other and all other sidebars from hybrid-core
  Added theme support for hybrid-core breadcrumbs and changed theme code to insert these breadcrumbs.
  Fixed footer_withbreadcrumbs.php to call hybrid-core breadcrumbs code - breadcrumb_trail.
  
0.1
  Oops! A sloppy copy/paste of files from original WFCollegeOne 1.7 overwrote the first readme.txt for WFCollege One Pro, so I have no idea what I did in version 0.1...