<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
			<?php if ( have_posts() ) : ?>

					<h1 class="page-title"><?php printf( __( 'Search Results for: %s' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

               
                    <?php while (have_posts()) : the_post(); ?>
                            <div class="entry">
	<?php the_title('<h2><a href="' .get_permalink(). '">', '</a></h2>'); ?>

                                <?php the_excerpt('Read the rest of this entry &raquo;'); ?>
                            </div>
                                           
                    <?php endwhile; ?>
			<?php else : ?>
                        <div class="post no-results not-found" id="post-0">
                        
                            <div class="entry">
                              <h1 class="page-title">Nothing Found</h1>

                                <p>Sorry, but nothing matched your search criteria. Please try again with different search keywords.</p>
                                <?php get_search_form(); ?>
                            </div>
                            
                        </div>
        <?php endif; ?>
    </div><!-- close wfCollegeOne -->
</div><!-- close MainContent -->

<?php get_footer(); ?>
