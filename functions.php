<?php

/* Load the core theme framework. */
require_once( trailingslashit( TEMPLATEPATH ) . 'hybrid-core/hybrid.php' );
$theme = new Hybrid();

/* Theme PHP code will go here. */
add_action( 'after_setup_theme', 'wf_college_pro_theme_setup', 10 );

function wf_college_pro_theme_setup() {

add_action( 'wp_enqueue_scripts', 'wfcopro_add_js');
function wfcopro_add_js() {
	wp_register_script( 'ddsmoothmenu', get_template_directory_uri() . 			'/js/ddsmoothmenu/ddsmoothmenu.js', '' , '1.5' );
	wp_enqueue_script( 'ddsmoothmenu' );
	wp_register_style ( 'ddsmoothmenu-h-css', get_template_directory_uri() . 	'/js/ddsmoothmenu.css', '' , '1.5' );
	wp_enqueue_style ( 'ddsmoothmenu-h-css' );
		wp_register_style ( 'ddsmoothmenu-v-css', get_template_directory_uri() . '/js/ddsmoothmenu-v.css', '', '1.5' );
	wp_enqueue_style ( 'ddsmoothmenu-v-css' );

}
	
/* automatic updater code from Konstructors */
	require_once('inc/update.php'); // Include automatic updater

	 include( trailingslashit( TEMPLATEPATH ) . 'inc/theme-options.php' );

// Remove blog-related Dashboard widgets, like the number of posts, number of comments, QuickPress, etc.
	if (! function_exists( 'remove_dashboard_cruft' ) ){
	// To put these dashboard items back in, call remove_dashboard_cruft in child theme.
	  function remove_dashboard_cruft() {
		global $wp_meta_boxes;
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
		unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
		unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);
		}
	}
	add_action('wp_dashboard_setup', 'remove_dashboard_cruft' );
 
	/* Theme-supported features go here. */
  add_theme_support( 'hybrid-core-widgets' );
  add_theme_support( 'hybrid-core-menus' );
  add_theme_support( 'hybrid-core-sidebars', array( 'primary', 'secondary', 'subsidiary', 'header', 'before-content', 'after-content', 'after-singular' ) );
	add_theme_support( 'hybrid-core-seo' );
	add_theme_support('get-the-image');
	
	/* Add theme support for WordPress features. */
	
	/* Added in version 1.0 of WF College One Pro, theme support for custom backgrounds. */
	
$args = array(
	'default-color' => 'FFFDE8',
);
add_theme_support( 'custom-background', $args );

	/* Add support for custom header images 
	* Copy this section to a child theme to override these settings. */
	$args = array(
		 'default-image'          => '%s/images/headers/WFwhitetolightgold.jpg',
		//'random-default'         => false,
		'width'                  => 960,
		'height'                 => 110,
		'header-text'            => '',
		'uploads'                => true,
		'wp-head-callback'       => 'wfcopro_header_style',
		'admin-head-callback'    => '',
		'admin-preview-callback' => '',
	);
	add_theme_support( 'custom-header', $args );
	/* End of custom header settings section */
/* Header text stuff copied from Twenty Twelve's inc/header-options.php	*/
/**
 * Styles the header text displayed on the blog.
 *
 * get_header_textcolor() options: 444 is default, hide text (returns 'blank'), or any hex value.
 *
 */
function wfcopro_header_style() {
	$text_color = get_header_textcolor();

	// If no custom options for text are set, let's bail
	if ( $text_color == get_theme_support( 'custom-header', 'default-text-color' ) )
		return;

	// If we get this far, we have custom styles.
	?>
	<style type="text/css">
	<?php
		// Has the text been hidden?
		if ( ! display_header_text() ) :
	?>
		#blogTitle,
		.site-description {
			position: absolute !important;
			clip: rect(1px 1px 1px 1px); /* IE7 */
			clip: rect(1px, 1px, 1px, 1px);
		}
	<?php
		// If the user has set a custom color for the text, use that.
		else :
	?>
		#blogTitle,
		.site-description {
			color: #<?php echo $text_color; ?> !important;
		}
	<?php endif; ?>
	</style>
	<?php
}
// End of header text stuff from Twenty Twelve's inc/header-options.php

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	// To reference the child theme's directory instead, use %2$s instead of %s below.
		register_default_headers( array(
			'WFU Black to Gold' => array(
				'url' => '%s/images/headers/WFblacktogold.jpg',
				'thumbnail_url' => '%s/images/headers/WFblacktogold-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU Black to Gold', 'wfcollegeone' )
			),
			'wfgraytoblack' => array(
				'url' => '%s/images/headers/WFgraytoblack.jpg',
				'thumbnail_url' => '%s/images/headers/WFgraytoblack-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU Gray to Black', 'wfcollegeone' )
			),
			'WFwhitetogold' => array(
				'url' => '%s/images/headers/WFwhitetogold.jpg',
				'thumbnail_url' => '%s/images/headers/WFwhitetogold-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU White to Gold', 'wfcollegeone' )
			),
			'wfwhitetolightgold' => array(
				'url' => '%s/images/headers/WFwhitetolightgold.jpg',
				'thumbnail_url' => '%s/images/headers/WFwhitetolightgold-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU White to Light Gold', 'wfcollegeone' )
			),
			'wfwhitetoblue' => array(
				'url' => '%s/images/headers/WFwhitetoblue.jpg',
				'thumbnail_url' => '%s/images/headers/WFwhitetoblue-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU White to Blue', 'wfcollegeone' )
			),
			'wflightgoldtoblack' => array(
				'url' => '%s/images/headers/WFlightgoldtoblack.jpg',
				'thumbnail_url' => '%s/images/headers/WFlightgoldtoblack-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU Light Gold to Black', 'wfcollegeone' )
			),
			'wfblacktogold_deptnamewhite' => array(
				'url' => '%s/images/headers/WFblacktogold_deptnamewhite.jpg',
				'thumbnail_url' => '%s/images/headers/WFblacktogold_deptnamewhite-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU Black to Gold-Dept. Name White', 'wfcollegeone' )
			),
			'wflightgoldtoblack_deptnamewhite' => array(
				'url' => '%s/images/headers/WFlightgoldtoblack_deptnamewhite.jpg',
				'thumbnail_url' => '%s/images/headers/WFlightgoldtoblack_deptnamewhite-thumbnail.jpg',
				/* translators: header image description */
				'description' => __( 'WFU Light Gold to Black-Dept Name White', 'wfcollegeone' )
			)
		) );
/* end of default custom header stuff */
	
	/* Add theme support for framework extensions. */
	add_theme_support( 'breadcrumb-trail' );

    /* Action and filters go here. */
	
	/* include filter for adding fields to WP Profile page,
	* for use on the Faculty Profile page template */
include( 'inc/facultyprofilefields.php' );

    /* create a custom post type of news, attached to the init hook.*/
  if (!function_exists('wfco_create_news_post_type')) {

    add_action( 'init', 'wfco_create_news_post_type' );
    function wfco_create_news_post_type() {
    
        $args = array(
          'labels' => array(
          'name' => __( 'News Items' ),
          'singular_name' => __( 'News Item' ),
          'add_new_item' => __( 'Add News Item' ),
          'edit_item' => __( 'Edit News Item' ),
          'new_item' => __( 'New News Item' ),
          'search_items' => __( 'Search News' )
          ),
          'public' => true,
          'has_archive' => 'news',
          'rewrite' => array('slug' => 'news'),
          'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt', 'revisions', 'comments', 'trackbacks'),
          'taxonomies' => array('news_type', 'post_tag'),
          'menu_position' => 20
      );
          register_post_type( 'wfco_dept_news', $args );
    }
    /* Create a custom taxonomy of news type and add it to the news post type*/
    $args = array(
      'label' => 'News Type',
      'hierarchical' => true
      );
      register_taxonomy('news_type', 'News Items', $args);
  }
}    

//This function, is_tree checks to see if a page is ANY ancestor of another page. Function is from http://codex.wordpress.org/Conditional_Tags
  function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors( $post->ID );
    foreach($anc as $ancestor) {
      if(is_page() && $ancestor == $pid) {
        return true;
      }
    }
    if(is_page()&&(is_page($pid)))
                 return true;   // we're at the page or at a sub page
    else
                 return false;  // we're elsewhere
  };

  /* added in version 1.1.3 */
/* set the size of the post thumbnails for all featured image locations */
set_post_thumbnail_size (150, 150);
