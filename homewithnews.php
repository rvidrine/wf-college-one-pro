<?php
  /*
  Template Name: Front Page with News Items
  */
?>
<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('news'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
                <?php if (have_posts()) : ?>
                
                    <?php while (have_posts()) : the_post(); ?>
                    
                        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                            <div class="entry">
	<?php the_title('<h1 class="page-title">', '</h1>'); ?>
			<!-- display list of sub-pages, if this is a top-level page with children -->
		<?php
  if($post->post_parent)
  $children = '';
  else
  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
  if ($children) { ?>
  <ul>
  <?php echo $children; ?>
  </ul>
  <?php } ?>

<!-- end of show children bit (from codex.wordpress.org) -->

                                <?php the_content('Read the rest of this entry &raquo;'); ?>
                            </div>
                            
                        </div>
                        
                    <?php endwhile; ?>
                    
                 <?php else : ?>
                 <?php endif; ?>
    </div>
</div>

<?php get_footer(); ?>
