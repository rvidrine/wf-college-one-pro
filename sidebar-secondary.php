<?php
  if ( is_active_sidebar('secondary') ) : { //if there are widgets ?>
  <div id="sidebar-secondary" class="sidebar">
    <ul>
      <li>
        <?php dynamic_sidebar(secondary); // display the widgets ?>
      </li>
    </ul>
  </div>
<?php  } else : { // do something if there aren't widgets ?> 
<!-- If you don't have any widgets in your secondary widget area, nothing will appear in the secondary sidebar. -->
  <?php };
endif; ?>
