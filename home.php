<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>

 <div id="mainContent">
    <div class="wfCollegeOne">
                <?php if (have_posts()) : ?>
                
                    <?php while (have_posts()) : the_post(); ?>
                    
                        <div <?php post_class() ?> id="post-<?php the_ID(); ?>">
                        
                            <div class="entry">
	<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail( 'thumbnail', array('class' => 'alignleft') );
} 
?>
	<?php the_title('<h2><a href="' .get_permalink(). '">', '</a></h2>'); ?>

                                <?php the_excerpt('Read the rest of this entry &raquo;'); ?>
                              <p class="categories">
                                <?php echo get_the_term_list( $post->ID, 'category', 'Related Posts: ', ' &bull; ', '' ); ?>
                              </p>
                            </div>
                            
                        </div>
                        
                    <?php endwhile; ?>
                    
                 <?php else : ?>
                 <?php endif; ?>
<div class="navigation">
	    <div class="alignleft"><?php next_posts_link('Previous 10 Posts') ?></div>
	    <div class="alignright"><?php previous_posts_link('Next 10 Posts') ?></div>
	</div>
    </div>
</div>

<?php get_footer(); ?>
