<?php
  /* Template Name: Blog page
  */
?>
<?php get_header(); ?>
<?php get_sidebar('primary'); ?>
<?php get_sidebar('secondary'); ?>

  <div id="mainContent">
    <div class="wfCollegeOne">

                <?php if (have_posts()) : ?>
                
                    <?php while (have_posts()) : the_post(); ?>
	<div class="entry-content">
    <?php the_title('<h2><a href="' .get_permalink(). '">', '</a></h2>'); ?>
        <span class="post_thumbnail">
        	<?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		?>
		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		<?php  the_post_thumbnail(); ?>
		</a>
		<?php } 	
		?>
        </span>
<?php	the_excerpt(); ?>
</div>
<?php endwhile; ?>
<?php endif; ?>
                    
    </div>
</div>
<div class="navigation">
	    <div class="alignleft"><?php next_posts_link('Previous 10 posts') ?></div>
	    <div class="alignright"><?php previous_posts_link('Next 10 posts') ?></div>
	</div>

<?php get_footer(); ?>
